/**
 * Created by Salaam Soleil on 3/15/14.
 */
import javafx.application.Application
import javafx.scene.Group
import javafx.scene.Scene
import javafx.*
import javafx.scene.chart.PieChart
import javafx.scene.layout.GridPane
import javafx.scene.paint.LinearGradient
import javafx.scene.paint.Paint
import javafx.scene.shape.Rectangle
import javafx.scene.shape.Shape
import javafx.stage.Stage

class AutoCell {
    //properties
    def blue = 0x00 //int('ff') //'00'
    def red = 0x00
    def green = 0x00
    def alpha = 0
    def neighborhoodProx = 1
    def neighborhoodType = ['border','radial']

    //grammars

    def brighter(i) {
        if (red + i < 255) {red += i} else {red = 255}
        if (green + i < 255) {green += i} else {green = 255}
        if (blue + i < 255) {blue += i} else {blue = 255}
    }

}
