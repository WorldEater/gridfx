package util

import javafx.scene.paint.Paint

/**
 * Created by Salaam Soleil on 3/15/14.
 */
class DrawUtils {
    static def setPaintVal(r,g,b) {
        def localPaint = "#${r.toHexString(r)}${g.toHexString(g)}${b.toHexString(b)}"
        localPaint = localPaint.replaceAll('0','00')
        Paint.valueOf(localPaint)
    }
}
