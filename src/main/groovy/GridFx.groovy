/**
 * Created by Salaam Soleil on 3/14/14.
 */
import javafx.application.Application
import javafx.scene.Group
import javafx.scene.Scene
import javafx.scene.chart.PieChart
import javafx.scene.paint.LinearGradient
import javafx.scene.shape.Rectangle
import javafx.stage.Stage
import javafx.scene.paint.Paint
import util.DrawUtils

class GridFx extends Application
{
    void start(Stage stage)
    {


        def init = new AutoCell()
        init.green = 0xff
        def group = new Group()
        def scene = new Scene(group, 800, 600)

        /* No more Pie
        def pie = new PieChart()
        pie.getData().addAll(new PieChart.Data("Cat1", 100), new PieChart.Data("Cat2", 200))
        group.getChildren().add(pie)
        */

        def box = new Rectangle()
        box.setWidth(200);
        box.setHeight(200);

        /*
        def firstVal = "#00ff00"
        def gradient = LinearGradient.valueOf("linear-gradient(from 0% 0% to 100% 100%, ${firstVal} 0%, 0xff000010 50%, 0x1122ff90 100%)")
        box.setFill(gradient)
        */

        def y = init.green.toHexString(init.green)

        def x = "#${init.red.toHexString(init.red)}${init.green.toHexString(init.green)}${init.blue.toHexString(init.blue)}"

        def paint = DrawUtils.setPaintVal(init.red,init.green,init.blue)

        //def paint = Paint.valueOf(x)
        box.setFill(paint)

        group.getChildren().add(box)

        def nextBox = new Rectangle()
        nextBox.setWidth(200)
        nextBox.setHeight(200)
        nextBox.setX(box.width)

        init.brighter(200)

        paint = DrawUtils.setPaintVal(init.red,init.green,init.blue)
        nextBox.setFill(paint)

        group.getChildren().add(nextBox)

        stage.setScene(scene)
        stage.show()
    }

    static void main(args)
    {
        Application.launch(GridFx.class, args)
    }
}
